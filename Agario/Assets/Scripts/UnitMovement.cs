﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnitMovement : MonoBehaviour {

    public float Speed;
    public float Size;

    public new Camera camera;

    public Text scoreText;
    public int score = 0;
    void Start()
    {
        Speed = 0.1f;
        Size = 1;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Food(Clone)")
        {
            Size += other.gameObject.GetComponent<EatFood>().Size;
            camera.orthographicSize = Size + 10;

            score += 1;
            scoreText.text = "Score: " + score;
            Destroy(other.gameObject);
        }

    }

    void Move()
    {
        Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Target.z = transform.position.z;
        transform.position = Vector3.Lerp(transform.position, Target, Speed / Size);
    }

    void Resize()
    {
        //to easily resize the player
        transform.localScale = new Vector3(1, 1, 1) * Size;
    }

    void Update()
    {
        Move();
        Resize();
    }
}
