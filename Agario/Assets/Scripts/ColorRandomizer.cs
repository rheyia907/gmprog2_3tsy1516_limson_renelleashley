﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorRandomizer : MonoBehaviour {

    public List<Material> Colors = new List<Material>();

    void Awake()
    {
        GetComponent<Renderer>().material = Colors[Random.Range(0, Colors.Count)];
    }
}
