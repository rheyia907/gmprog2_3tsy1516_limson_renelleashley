﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AISpawn : MonoBehaviour {

    private List<GameObject> AIcount = new List<GameObject>();
    public GameObject AI;  
    public int CurrentSpawnCount;
    public int MaximumSpawnCount;

    public float spawnTime = 3f;         
    public float timer = 0.0f;

    bool spawn = true;

    void Update()
    {
        // SPAWNS EVERY 3 SECONDS
        if (spawn)
        {
            timer += Time.deltaTime;

            if (timer > spawnTime)
            {
                if (CurrentSpawnCount < MaximumSpawnCount)
                {
                    Spawn();
                    CheckSpawnedStatus();
                    timer = 0.0f;
                }
                else
                {
                    spawn = false;
                }
            }
        }
        // ================
        
    }

    void CheckSpawnedStatus()
    {
        for(int x = 0; x < AIcount.Count; x++)
        {
            if(AIcount[x] == null)
            {
                CurrentSpawnCount--;
                AIcount.RemoveAt(x);
            }
        }
    }
    void Spawn()
    {
        GameObject tempAIholder;
        int x = Random.Range(-50, Camera.main.pixelWidth + 50);
        int y = Random.Range(-50, Camera.main.pixelHeight + 50);

        Vector3 Target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10));
        Target.z = 10;
        tempAIholder = (GameObject)Instantiate(AI, Target, Quaternion.identity);
        AIcount.Add(tempAIholder);
        CurrentSpawnCount = AIcount.Count;
    }

}
