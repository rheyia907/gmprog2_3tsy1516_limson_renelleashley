﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour {

    protected virtual void Initialize() { }
    protected virtual void FSMUpdate() { }

    public enum STATE
    {
        Patrol,
        Chase,
        Run
    };

    public STATE curState;
    public STATE prevstate;

}
