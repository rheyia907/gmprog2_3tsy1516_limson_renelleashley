﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Enemy : StateMachine
{
    public float playerSize;
    public float size;
    public float speed;
    public AISpawn spawn;

    public ObjectDetection detectedObject; 
    public Vector3 randomDirection = new Vector3();
    void Start()
    {
        Initialize();
    }

    void Update()
    {
        FSMUpdate();
        Resize();
    }

    public void ChangeState(STATE newState)
    {
        curState = newState;
    }
    
    protected override void Initialize()
    {
        base.Initialize();

        randomDirection = new Vector3(RandomNumber(-50, 50), RandomNumber(-50, 50), 10);
        prevstate = STATE.Patrol;
        curState = STATE.Patrol;
    }
    protected override void FSMUpdate()
    {
        base.FSMUpdate();
        switch (curState)
        {
            case STATE.Patrol:
                UpdatePatrolState();
                break;
            case STATE.Chase:
                UpdateChaseState();
                break;
            case STATE.Run:
                UpdateRunState();
                break;
        }
    }

    protected void UpdatePatrolState()
    {

        if (prevstate != curState)
        {
            prevstate = curState;
        }
        this.transform.position = Vector3.MoveTowards(transform.position, randomDirection, speed / size * Time.deltaTime);
        if (Vector3.Distance(this.transform.position, randomDirection) <= 0.1f)
        {
            Debug.Log("patrol");  
            randomDirection = new Vector3(RandomNumber(-50, 50), RandomNumber(-50, 50), 10  );
        }

        if(detectedObject.nearByEnemy != null)
        {
            curState = STATE.Chase;
        }
            

    }

    protected void UpdateChaseState()
    {
        if (prevstate != curState)
        {
            prevstate = curState;
        }
        Debug.Log("hur");

        if (detectedObject.nearByEnemy == null)
        {
            curState = STATE.Patrol;
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, detectedObject.nearByEnemy.transform.position, speed / size * 2 * Time.deltaTime);
        }
       
    }
    
    protected void UpdateRunState()
    {
        if (prevstate != curState)
        {
            prevstate = curState;
        }
        Debug.Log("allahu akbar");
        transform.position = Vector3.MoveTowards(transform.position, -1 * detectedObject.nearByEnemy.transform.position, speed / size * 2 * Time.deltaTime);

    }


    int RandomNumber(int min, int max)
    {
        return Random.Range(min, max);
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.name == "Food(Clone)")
        {
            size += other.gameObject.GetComponent<EatFood>().Size;
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Player")
        {
            float playerSize = other.gameObject.GetComponent<UnitMovement>().Size;
            if (playerSize < size)
            {
                Debug.Log("i eat player");
                Destroy(other.gameObject);
                SceneManager.LoadScene("Main");
            }
            if(playerSize > size)
            {
                spawn = GetComponent<AISpawn>();
                Debug.Log("player eat me");
                Destroy(this.gameObject);
            }

        }
    }

    void Resize()
    {
        transform.localScale = new Vector3(1, 1, 2) * size;
    }
}
