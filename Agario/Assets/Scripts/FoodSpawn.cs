﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodSpawn : MonoBehaviour {

    public GameObject Food;  // Dapat List :)
    public float SpawnSpeed;
    
    void Start()
    {
        InvokeRepeating("GenerateFood", 0, SpawnSpeed);
    }

    void GenerateFood()
    {
        int x = Random.Range(-50, Camera.main.pixelWidth + 50);
        int y = Random.Range(-50, Camera.main.pixelHeight + 50);

        Vector3 Target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10));
        Target.z = 10;

        Instantiate(Food, Target, Quaternion.identity);
    }
	
}
