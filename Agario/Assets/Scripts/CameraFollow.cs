﻿ using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public GameObject Player;

    private Vector3 offset;
    public float minimum = 0.0f;
    public float maximum = 1.0f;



    void Start()
    {
        offset = transform.position - Player.transform.position;
    }

    void Update()
    {
        if (Player == null)
        {
            return;
        }

        transform.position = Player.transform.position + offset;
        transform.position = new Vector3(
        Mathf.Clamp(transform.position.x, minimum, maximum),
        Mathf.Clamp(transform.position.y, minimum, maximum),
        transform.position.z);

       
    }
}
