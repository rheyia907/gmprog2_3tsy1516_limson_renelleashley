﻿using UnityEngine;
using System.Collections;

public class HealthSpawn : MonoBehaviour {

    public GameObject healthPrefab;
    public float spawnTime;

    void Start()
    {
        InvokeRepeating("SpawnHealth", 2, spawnTime);
    }

    void SpawnHealth()
    {
        GameObject bonusHealth = (GameObject)Instantiate(healthPrefab, new Vector3(Random.Range(-5, 5), 10, 2), Quaternion.identity);
    }
}
