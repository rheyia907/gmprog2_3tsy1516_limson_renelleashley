﻿using UnityEngine;
using System.Collections;

public class FallingHealth : MonoBehaviour {

    public float healthSpeed;
    public float healthRotation;



    void Update()
    {
        this.transform.Translate(Vector3.down * healthSpeed * Time.deltaTime);
        transform.Rotate(Vector3.up * (healthRotation * Time.deltaTime));
        Destroy(this.gameObject, 15);
    }
}
