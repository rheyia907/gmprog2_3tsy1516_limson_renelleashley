﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

	public GameObject projectilePrefab;
	public float projectileSpeed;



	void Update () 
	{
	
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			GameObject bullet =  (GameObject)Instantiate (projectilePrefab, this.transform.position, Quaternion.identity);
			Destroy (bullet, 0.7f);
		}

	}
}
