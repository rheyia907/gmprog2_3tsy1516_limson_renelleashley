﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {

	public float speed;
    public float maxHealth;
    public float curHealth;
    public Image shipHealth;

    void Start()
    {
        curHealth = maxHealth;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Asteroid(Clone)")
        {
            curHealth -= 1;
            Destroy(other.gameObject);
            shipHealth.fillAmount = curHealth/maxHealth;

            if (curHealth <= 0)
            {
                Destroy(this.gameObject);
				SceneManager.LoadScene ("Main");
            }
        }
    }
    
    void Movement()
	{
		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			transform.Translate(Vector3.left * speed * Time.deltaTime);
		} 

		else if (Input.GetKey (KeyCode.RightArrow))
		{
			transform.Translate(Vector3.right * speed * Time.deltaTime);
		}

    }
	void Update ()
	{
		Movement ();
	}
}
