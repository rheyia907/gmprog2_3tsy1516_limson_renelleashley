﻿using UnityEngine;
using System.Collections;

public class BulletMovement : MonoBehaviour {

	public float BulletSpeed;
    public PlayerMovement playerMovement;

    void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
    }

	void Update()
	{
		this.transform.Translate (Vector3.up * BulletSpeed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider other)
	{
		if ((other.gameObject.name == "Asteroid(Clone)") || (other.gameObject.name =="SmallAsteroid(Clone)"))
		{
			Destroy (this.gameObject);
			Destroy (other.gameObject);
		}

        if ((other.gameObject.name == "BonusHealth(Clone)"))
        {
            Destroy(this.gameObject);
            Destroy(other.gameObject);
            playerMovement.curHealth++;
    
        }
    }
}
