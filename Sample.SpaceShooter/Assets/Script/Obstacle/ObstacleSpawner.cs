﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleSpawner : MonoBehaviour {

	public GameObject asteroidPrefab;
    public float spawnTime;

	void Start()
    {
        InvokeRepeating("SpawnAsteroid", 2, spawnTime);
    }

    void SpawnAsteroid()
    {
        GameObject asteroid = (GameObject)Instantiate(asteroidPrefab, new Vector3(Random.Range(-5, 5), 10, 1.5f), Quaternion.identity);
    }


}
