﻿using UnityEngine;
using System.Collections;

public class ObstacleMovement : MonoBehaviour {

	public float asteroidSpeed;
	public float asteroidRotation;

    

  	void Update ()
	{
		this.transform.Translate (Vector3.down * asteroidSpeed * Time.deltaTime);
		transform.Rotate (Vector3.up * (asteroidRotation * Time.deltaTime));
        Destroy(this.gameObject, 4);
	}

	
}
