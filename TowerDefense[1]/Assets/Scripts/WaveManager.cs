﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour {
    float spawnCD = 0.1f;
    float spawnCDRemaining = 0;

    [System.Serializable]
    public class WaveComponent
    {
        public GameObject enemyPrefab;
        public int num;
        [System.NonSerialized]
        public int spawned = 0;
    }


    public WaveComponent[] waveComps;

    void Update()
    {
        bool didSpawn = false;
        spawnCDRemaining -= Time.deltaTime;
        if (spawnCDRemaining < 0)
        {
            spawnCDRemaining = spawnCD;

            foreach (WaveComponent wc in waveComps)
            {
                if (wc.spawned < wc.num)
                {
                    wc.spawned++;
                    Instantiate(wc.enemyPrefab, this.transform.position, this.transform.rotation);
                    didSpawn = true;
                    break;
                }
            }

            if (didSpawn == false)
            {
                Destroy(gameObject);
            }
        }
    }
}
