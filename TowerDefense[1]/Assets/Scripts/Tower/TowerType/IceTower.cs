﻿using UnityEngine;
using System.Collections;

public class IceTower : Tower {

	void Start () {
        base.Start();
	}

    public override void ShootAt(Enemy e)
    {
        radius = 5;
        base.ShootAt(e);
    }
}
