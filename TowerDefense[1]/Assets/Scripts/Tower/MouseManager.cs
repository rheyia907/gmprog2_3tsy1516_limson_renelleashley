﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MouseManager : MonoBehaviour {

    public GameObject selectedObject;
    public Text objName;
    public Text objInfo;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //  Debug.Log("Mouse is down");

            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                //Debug.Log("Hit " + hitInfo.transform.gameObject.name);
                if (hitInfo.transform.gameObject.tag == "Tower")
                {
                    Debug.Log("It's working!");

                }
                selectedObject = hitInfo.transform.gameObject;
                objName.text = "Name: " + selectedObject.name.ToString();
                objInfo.text = "Info: ";
                   
            }
            else {
                Debug.Log("No hit");
                selectedObject = null;
                objName.text = "Name: Nothing Is Selected";
            }
            //  Debug.Log("Mouse is down");
        }
    }
}
