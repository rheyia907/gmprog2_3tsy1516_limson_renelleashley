﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Builder : MonoBehaviour
{

    public GameObject target;
    public float speed;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Tower");
    }
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
    }
}
