﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        GameObject life = GameObject.FindGameObjectWithTag("ScoreManager");
        life.GetComponent<ScoreManager>().LoseLife();
        Destroy(other.gameObject);
    }

}
