﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public int health = 0;
    public int maxhealth;
    public int moneyValue = 0;

    public Image healthBar;

    void Start()
    {
        health = maxhealth;
        healthBar = transform.FindChild("EnemyCanvas").FindChild("HealthGB").FindChild("Health").GetComponent<Image>();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        healthBar.fillAmount = health / maxhealth;
        if (health <= 0)
        {
            Die();
        }

    }

    public void hpIncrease(float multiplier, int waveCount)
    {

    }

    public void goldIncrease(float multiplier, int waveCount)
    {

    }

    public void Die()
    {
        FindObjectOfType<ScoreManager>().money += moneyValue;
        Destroy(gameObject);
    }
}
