﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour {


    public enum SpawnState { SPAWNING, WAITING, COUNTING };

    [System.Serializable]
    public class wave
    {
        public Transform enemy;
        public int count;
        public float rate;
    }

    public wave[] waves;
    private int nxtwave = 0;
    public float timeBetweenWaves;
    public float waveCD;

    public Text waveCountTxt;
    public Text waveCountDownTxt;


    private float SearchEnemy = 1f;

    void Start()
    {
        waveCD = timeBetweenWaves;
     
    }

    private SpawnState state = SpawnState.COUNTING;

    void Update()
    {
        if (waveCD != 0)
        {
            waveCountDownTxt.text = "Prepare your defenses : " + waveCD + " seconds left";
        }
        else
        {
            waveCountDownTxt.enabled = false;
        }

        collapse();
      
        if (state == SpawnState.WAITING)
        {
            if (!EnemyAlive())
            {
                WaveComplete();
            }
            else
            {
                return;
            }
        }

        if (waveCD <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(Spawnwave(waves[nxtwave]));
            }
        }
        else
        {
            waveCD -= Time.deltaTime;
        }
    }

    void WaveComplete()
    {
        Debug.Log("Done");
        state = SpawnState.COUNTING;
        waveCD = Time.deltaTime;

        if (nxtwave + 1 > waves.Length - 1)
        {
            nxtwave = 0;
        }
        else
        {
            nxtwave++;
            UI();
        }

    }

    bool EnemyAlive()
    {
        SearchEnemy -= Time.deltaTime;
        if (SearchEnemy <= 0f)
        {
            SearchEnemy = 1f;
            if (GameObject.FindGameObjectWithTag("Monster") == null)
            {
                return false;
            }
        }

        return true;
    }

    IEnumerator Spawnwave(wave Wave)
    {
        state = SpawnState.SPAWNING;
        for (int i = 0; i < Wave.count; i++)
        {
            SpawnEnemy(Wave.enemy);
        }


        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform enemy)
    {
        Debug.Log("Spawning");
        Instantiate(enemy, this.transform.position, Quaternion.identity);
    }

    void collapse()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (GameObject ground in GameObject.FindGameObjectsWithTag("Monster"))
            {
                Destroy(ground);
            }

        }
    }


    void UI()
    {
       
        waveCountTxt.text = "Wave : " + nxtwave;
    }
}
