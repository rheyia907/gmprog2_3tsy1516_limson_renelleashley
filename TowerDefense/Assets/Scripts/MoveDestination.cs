﻿using UnityEngine;
using System.Collections;

public class MoveDestination : MonoBehaviour
{

    public GameObject goalDestination;
    NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //moves object to set destination
        agent.destination = goalDestination.transform.position;
    }

    void Update()
    {
       // CheckIfGoalReached();
    }

   

    //void CheckIfGoalReached()
    //{
    //    //if the object reached the destination, destroy the object
    //    if (agent.remainingDistance <= 1)
    //    {
    //        Destroy(gameObject);
    //    }
    //}

    //void OnDestroy()
    //{
    //    GameObject life = GameObject.FindGameObjectWithTag("ScoreManager");
    //    life.GetComponent<ScoreManager>().LoseLife();
    //}


}
