﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {

    public int lives = 100;
    public int money = 100 ;


    public Text moneyText;
    public Text livesText;

    public void LoseLife()
    {
        Debug.Log("LOSE LFIE");
        lives -= 1;
        if (lives <= 0)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        Debug.Log("GameOver");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void Update()
    {
        moneyText.text = money.ToString();
        livesText.text = lives.ToString();
    }
}
