﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public int health = 0;
    public int maxhealth;
    public int moneyValue = 0;

    public float duration;

    public bool slow = false;
    public bool DOT = false;
    private float InitialSpeed;
    float timeElapsed;
    

    //public Image healthBar;

    void Start()
    {
        health = maxhealth;
        //healthBar = transform.FindChild("EnemyCanvas").FindChild("HealthGB").FindChild("Health").GetComponent<Image>();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        //healthBar.fillAmount = health / maxhealth;
        if (health <= 0)
        {
            Die();
        }

    }

    public void hpIncrease(int multiplier, int waveCount)
    {
        health = multiplier * waveCount + health;
    }

    public void goldIncrease(int multiplier, int waveCount)
    {
        moneyValue = multiplier * waveCount + moneyValue;
    }

    public void Die()
    {
        FindObjectOfType<ScoreManager>().money += moneyValue;
        Destroy(gameObject);
    }
}
