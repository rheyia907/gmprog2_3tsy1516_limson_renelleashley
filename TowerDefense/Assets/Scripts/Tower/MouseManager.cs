﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MouseManager : MonoBehaviour
{

    public GameObject selectedObject;
    private GameObject nextUpgrade;

    public Text objInfo;

    bool isSelected;

    Tower twr;
    ScoreManager scoremanager;

    void Start()
    {
        isSelected = false;
    }

    void Update()
    {

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, LayerMask.NameToLayer("Tower")))
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("clicked");
                if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    //Debug.Log("Selected");
                    selectedObject = hit.collider.gameObject;
                    //gets the upgradeTo object in tower base class and sets it to next upgrade.
                    nextUpgrade = selectedObject.GetComponent<Tower>().upgradeTo;
                    DisplayInfo();
                }
                
            }
        }
        else
        {
            selectedObject = null;
        }
    }

    void DisplayInfo()
    {

        objInfo.text = "Name: " + selectedObject.name.ToString() +
            "\nCost: " + selectedObject.GetComponent<Tower>().price.ToString() +
            "\nDamage:" + selectedObject.GetComponent<Tower>().bulletPrefab.GetComponent<Bullet>().damage.ToString() +
            "\nFire Rate:" + selectedObject.GetComponent<Tower>().fireRate.ToString();
    }


    public void UpgradeTower()
    {
        Debug.Log("Tower Upgraded!");

        Instantiate(nextUpgrade, selectedObject.transform.position, Quaternion.identity);
        Destroy(selectedObject.gameObject);
    }
}
