﻿using UnityEngine;
using System.Collections.Generic;

public class BuildManager : MonoBehaviour {

	Ray ray;
	RaycastHit hit;
    Collider col;
    Builder b;
    [SerializeField] GameObject objectTower;
    [SerializeField]
    ScoreManager scoreManager;
    public List<GameObject> Towers = new List<GameObject>();

    public List<GameObject> Builders = new List<GameObject>();
    public GameObject spawnTent;
    public float speed = 1.0f;
    void Start () {
	}

	void Update () {
		SelectArea();
	}

	public void CreateTower(int index)
	{
        GameObject twr = Towers[index];
        if (scoreManager.money >= twr.GetComponent<Tower>().price)
        {
            scoreManager.money -= twr.GetComponent<Tower>().price;
            twr = Instantiate(twr) as GameObject;
            objectTower = twr;
        }

	}

	Vector3 SnapToGrid(Vector3 towerObject)
	{
		return new Vector3(Mathf.Round(towerObject.x),
							towerObject.y,
							Mathf.Round(towerObject.z));
	}

    void SelectArea()
    {

        if (objectTower != null)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 towerPos = hit.point;
                objectTower.transform.position = SnapToGrid(towerPos);

                if (hit.point.y > 2.2)
                {
                    objectTower.GetComponent<Tower>().Buildable();

                    if (Input.GetMouseButtonDown(0))
                    {                                      
                            CallBuilder(0);
                            objectTower.GetComponent<Tower>().Build();
                            col = objectTower.GetComponent<Collider>();
                            col.enabled = true;
                            objectTower.GetComponent<Tower>().building = true;
                            //  b.Moveto(towerPos);x
                            objectTower = null;
                        
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        Destroy(objectTower);
                    }
                }
                else
                    objectTower.GetComponent<Tower>().NonBuildable();

                Debug.DrawLine(ray.origin, hit.point, Color.red);
            }
        }
    }

    void CallBuilder(int i)
    {
        Instantiate(Builders[i], spawnTent.transform.position, Quaternion.identity);
    }
}
