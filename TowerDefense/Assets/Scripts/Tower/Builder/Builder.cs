﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Builder : MonoBehaviour
{

    public Vector3 mtarget;
    public float speed;

    public void Moveto(Vector3 target)
    {
        mtarget = target;
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, mtarget, step);
    }
}
