﻿using UnityEngine;
using System.Collections;

public class Ice : Bullet
{

    public float slowDuration = 5.0f;
    public float speedReducedTo = 0.5f;
    public float initialSpeed;

    void Start()
    {
        initialSpeed = target.GetComponent<NavMeshAgent>().speed;
    }



    public override void Debuff()
    {
        slowDuration -= Time.deltaTime;
        if (slowDuration <= 0)
        {
            target.GetComponent<NavMeshAgent>().speed = initialSpeed;
        }
        else
        {
            target.GetComponent<NavMeshAgent>().speed = speedReducedTo;
            Debug.Log("Reduced");
        }

        base.Debuff();
    }
}
