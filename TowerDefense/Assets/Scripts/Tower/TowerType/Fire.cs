﻿using UnityEngine;
using System.Collections;

public class Fire : Bullet{

    public float damageOvertimeDuration;
    public int DoT;

    void Start()
    {
        DoT = Mathf.RoundToInt(Time.deltaTime);
    }

    public override void Debuff()
    {
        damageOvertimeDuration -= Time.deltaTime;
        if (damageOvertimeDuration <= 0)
        {
           
        }
        else
        {
            target.GetComponent<Enemy>().health -= damage * DoT;
            Debug.Log("being damaged");
        }
        base.Debuff();
    }
}
