﻿using UnityEngine;
using System.Collections;

public class FireTower : Tower {

	// Use this for initialization
	void Start () {
        base.Start();
	}

    public override void ShootAt(Enemy e)
    {
        radius = 5;
        base.ShootAt(e);
    }
}
