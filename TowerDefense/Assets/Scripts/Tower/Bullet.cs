﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    //bullet / projectile

    public float speed = 15;
    public Transform target;
    public int damage = 1;
    public float radius;

   

    void Update()
    {
        ProjectileShoot();
    }



    public void ProjectileShoot()
    {
        //If tower shoots but no enemies, destroy bullet
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - this.transform.localPosition;

        float distThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distThisFrame)
        {
            DoBulletHit();
           
        }

        else
        {
            transform.Translate(dir.normalized * distThisFrame, Space.World);
            Quaternion targetRotation = Quaternion.LookRotation(dir);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 5);

        }
    }
 
    void DoBulletHit()
    {
        //FOR ARROW TOWER
        if (radius == 0)
        {
            GameObject enemy = GameObject.FindGameObjectWithTag("Monster");
            enemy.GetComponent<Enemy>().TakeDamage(damage);
            Debuff();

        }
        //FOR OTHER SPLASH TOWERS
        else
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, radius);

            foreach(Collider c in cols)
            {
                Enemy e = c.GetComponent<Enemy>();
                if(e != null)
                {
                    e.GetComponent<Enemy>().TakeDamage(damage);
                    Debuff();
                }
            }
        }
        Destroy(gameObject);
    }


    public virtual void Debuff()
    {

    }
}
