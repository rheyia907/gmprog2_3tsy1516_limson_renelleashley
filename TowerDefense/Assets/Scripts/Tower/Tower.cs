﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{
    
	[SerializeField]Material towerMat;
    Transform turretTransform;

 
    public GameObject bulletPrefab;
    public GameObject upgradeTo;


    public int price;
    public float buildDuration = 0;
    public float fireRate = 1.0f;

    public float radius;
    public float damage;

    private float Firetimeleft = 0;
    private float Range = 10;

    public bool building = true;

        
   public void Start()
    {
        turretTransform = transform.Find("Turret");
    }

    void Update()
    {

        if (building)
        {
            //print("BUILDING");
            buildDuration -= Time.deltaTime;
            this.towerMat.color = Color.blue;
        }
      
        if (buildDuration <= 0)
        {
            building = false;
            
            this.towerMat.color = Color.white;
            //will return a list of enemies in the game 
            Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

            Enemy nearestEnemy = null;
            float dist = Mathf.Infinity;

            foreach (Enemy e in enemies)
            {
                float d = Vector3.Distance(this.transform.position, e.transform.position);
                if (nearestEnemy == null || d < dist)
                {
                    nearestEnemy = e;
                    dist = d;
                }
            }

            if (nearestEnemy == null)
            {
                //Debug.Log("No enemies");
                return;
            }

            Vector3 dir = nearestEnemy.transform.position - this.transform.position;
            //checks if enemy is still in range. if yes, turret turns direction to nearest enemy.
            if (dir.magnitude <= Range)
            {
                Quaternion lookRot = Quaternion.LookRotation(dir);
                turretTransform.rotation = Quaternion.Euler(0, lookRot.eulerAngles.y, 0);
            }

            Firetimeleft -= Time.deltaTime;
            //only fires when target is within range and fire cooldown reaches 0
            if (Firetimeleft <= 0 && dir.magnitude <= Range)
            {
                Firetimeleft = fireRate;
                ShootAt(nearestEnemy);
                if(bulletPrefab.gameObject.tag == "Ice")
                {
                    nearestEnemy.GetComponent<Enemy>().slow = true;
                }
                else if (bulletPrefab.gameObject.tag == "Fire")
                {
                    nearestEnemy.GetComponent<Enemy>().DOT = true;  
                }

            }
        }

        else
        {
            this.towerMat.color = Color.blue;
        }
          

    }

      
   public virtual void ShootAt(Enemy e)
    {

        GameObject bulletObject = (GameObject)Instantiate(bulletPrefab, this.transform.position, Quaternion.identity);
        Bullet b = bulletObject.GetComponent<Bullet>();
        b.target = e.transform;
        b.radius = radius;
        if (e = null)
        {
            return;
        }
    }

    public void Buildable()
    {
       this. towerMat.color = Color.green;
    }

    public void NonBuildable()
    {
        this.towerMat.color = Color.red;
    }

    public void Build()
    {
        this.towerMat.color = Color.white;
    }


}


